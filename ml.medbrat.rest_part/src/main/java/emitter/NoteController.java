package emitter;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/") //потенциальная ошибка
public class NoteController {
    final AmqpTemplate template;

    public NoteController(@Autowired AmqpTemplate template) {
        this.template = template;
    }


    @RequestMapping(value = "/persist", method = RequestMethod.POST)
    public ResponseEntity<?> producer(@RequestBody String note) {
        String msgToSend = parseNote(note);
        try{
            System.out.println(msgToSend);

            template.convertAndSend("queue1", msgToSend);
            return ResponseEntity.ok().build();

        }catch(Exception e){
            return ResponseEntity.badRequest().build();
        }
    }

    private String parseNote(String jsonMessage){
        return jsonMessage.split("=")[1];
    }
}

