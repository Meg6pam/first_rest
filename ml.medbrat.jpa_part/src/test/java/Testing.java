import listener.NoteEntity;
import org.junit.jupiter.api.Test;
import org.springframework.stereotype.Controller;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@Controller
public class Testing {

    public static final String MESSAGE = "this is your message";

    @Test
    public void entityTest(){
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("thePersistenceUnit");
        EntityManager manager = factory.createEntityManager();
        assertNotNull(manager);

        manager.getTransaction().begin();
        NoteEntity entity = new NoteEntity(MESSAGE);
        manager.persist(entity);
        manager.getTransaction().commit();
        System.out.println(entity.getId());

        NoteEntity n = manager.find(NoteEntity.class, 1L);
        System.out.println(n.getMessage());

        assertNotNull(n);
        assertEquals(n.getMessage(), MESSAGE);
    }
}