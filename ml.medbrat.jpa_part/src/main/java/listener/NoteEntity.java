package listener;

import lombok.Data;

import javax.persistence.*;

@Data
@Table(name = "messages")
@Entity
public class NoteEntity {
    @Id
    @GeneratedValue
    private Long id;

    @Column(name="message1")
    private String message;

    public NoteEntity() {
    }

    public NoteEntity(String message) {
        this.message = message;
    }
}
