package listener;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;

@Controller
public class MessageReciever {

    private String note;

    private final NoteRepository noteRepository;

    private final ConnectionFactory connectionFactory;

    public MessageReciever(@Autowired NoteRepository noteRepository, @Autowired ConnectionFactory connectionFactory) {
        this.noteRepository = noteRepository;
        this.connectionFactory = connectionFactory;
    }

    @Bean
    public SimpleMessageListenerContainer messageListenerContainer() {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.setQueueNames("queue1");
        container.setMessageListener(new MessageListener() {
            //тут ловим сообщения из queue1

            public void onMessage(Message message) {
                note = new String(message.getBody());
                System.out.println("received from queue1 : " + note);
                noteRepository.save(new NoteEntity(note));
            }
        });
        return container;
    }

}
